package com.agiletestingalliance;
import static org.junit.Assert.*;
import org.junit.Test;

public class MinMaxTest{

    @Test
    public void testMax1() throws Exception {
        //calling add method
        int k= new MinMax().max(5,1);
        assertEquals("max test second number", 5, k);
    }

        @Test
    public void testMax2() throws Exception {
        //calling add method
        int k= new MinMax().max(4,9);
        assertEquals("max test first number", 9, k);
    }

        @Test
    public void testBarString() throws Exception {
        //calling add method
        String result= new MinMax().bar("something");
        assertEquals("test string", "something", result);
    }

            @Test
    public void testBarNull() throws Exception {
        //calling add method
        String result= new MinMax().bar(null);
        assertEquals("test null", null, result);
    }

               @Test
    public void testBarEmpty() throws Exception {
        //calling add method
        String result= new MinMax().bar("");
        assertEquals("test null", "", result);
    }


}